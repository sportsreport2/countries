import React, { PureComponent } from 'react';

class CountriesTable extends PureComponent {
    static getTableCountInfo(numberOfCountries, recordsTotal) {
        return "Showing " + numberOfCountries + (numberOfCountries > 1 ? " countries" : " country") + (numberOfCountries !== recordsTotal ? " (filtered from " + recordsTotal + " total countries)" : "");
    }

    render() {
        let { countries, recordsTotal } = this.props;

        if(countries.length > 0) {
            return (
                <div className="row marginTop10">
                    <div className="col-md-12">
                        <p>
                            {CountriesTable.getTableCountInfo(countries.length, recordsTotal)}
                        </p>
                        <table className="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Full Name</th>
                                <th>Alpha Code 2</th>
                                <th>Alpha Code 3</th>
                                <th>Flag</th>
                                <th>Region</th>
                                <th>Sub-Region</th>
                                <th>Population</th>
                                <th>Languages</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                countries.map(country => {
                                    return (
                                        <tr key={country.name}>
                                            <td>{country.name}</td>
                                            <td>{country.alpha2Code}</td>
                                            <td>{country.alpha3Code}</td>
                                            <td>
                                                <a href={country.flag} target="_blank">
                                                    <img src={country.flag} alt={country.name} className="img-responsive" width="50" />
                                                </a>
                                            </td>
                                            <td>{country.region}</td>
                                            <td>{country.subregion}</td>
                                            <td>{country.population.toLocaleString("en")}</td>
                                            <td>
                                                {country.languages.map(language => (<p key={country.name + language.name}>{language.name}</p>))}
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
            );
        }

        return '';
    }
}

export default CountriesTable;