import React, { PureComponent } from 'react';

class RegionCounts extends PureComponent {
    printRegions(){
        let { regionCounts } = this.props;

        let regions = Object.keys(regionCounts);
        regions.sort();

        return regions.map(region => {
            let rows = [];

            rows.push(<tr>
                <td>{region}</td>
                <td className="text-right">{regionCounts[region].count}</td>
            </tr>);

            let subregions = Object.keys(regionCounts[region].subregions);
            subregions.sort();

            rows.push(subregions.map(subregion => (
                    <tr>
                        <td className="paddingLeft45">{subregion}</td>
                        <td className="text-right">{regionCounts[region].subregions[subregion].count}</td>
                    </tr>
                )
            ));

            return (rows)
        })
    }

    render() {
        let { regionCounts } = this.props;

        if(Object.keys(regionCounts).length > 0) {
            return (
                <div className="row marginTop10">
                    <div className="col-md-4">
                        <table className="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Region</th>
                                <th>Country Count</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.printRegions()}
                            </tbody>
                        </table>
                    </div>
                </div>
            );
        }

        return '';
    }
}

export default RegionCounts;