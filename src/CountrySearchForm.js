import React, { PureComponent } from 'react';
import FormErrors from './FormErrors';

class CountrySearchForm extends PureComponent {
    static countryInputPlaceholder(searchType) {
        let countryInputPlaceholder = "Country ";
        switch(searchType) {
            case "name":
                return countryInputPlaceholder + "Name";
                break;

            case "alpha":
                return countryInputPlaceholder += "Code";
                break;

            default:
                return "Country"
        }
    }

    render() {
        let { handleFormSubmit, handleInputChange, formErrors, countryInput, searchType } = this.props;

        return (
            <div className="row">
                <div className="col-md-6">
                    <FormErrors errors={formErrors} />
                    <form onSubmit={handleFormSubmit} className="form-inline">
                        <div className={`form-group ${(formErrors.length === 0 ? '' : 'has-error')}`}>
                            <label htmlFor="countryInput">Search:</label>
                            <select className="form-control marginLeft5" name="searchType" value={searchType} onChange={handleInputChange}>
                                <option value="name">Name</option>
                                <option value="alpha">Code</option>
                            </select>
                            <input type="text" className="form-control marginLeft10" id="countryInput" name="countryInput" placeholder={CountrySearchForm.countryInputPlaceholder(searchType)} autoComplete="off" onChange={handleInputChange} value={countryInput} />
                        </div>
                        <button type="submit" name="submitBtn" className="btn btn-default marginLeft10" disabled={formErrors.length !== 0}>
                            Submit
                        </button>
                    </form>
                </div>
            </div>
        );
    }
}

export default CountrySearchForm;