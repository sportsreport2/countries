import React from 'react';
import ReactDOM from 'react-dom';
import CountrySearchForm from './CountrySearchForm';

test('country input placeholder when search type = name', () => {
    expect(CountrySearchForm.countryInputPlaceholder("name")).toBe("Country Name");
});

test('country input placeholder when search type = alpha', () => {
    expect(CountrySearchForm.countryInputPlaceholder("alpha")).toBe("Country Code");
});

test('country input placeholder when search type = blank', () => {
    expect(CountrySearchForm.countryInputPlaceholder("")).toBe("Country");
});