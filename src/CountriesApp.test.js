import React from 'react';
import ReactDOM from 'react-dom';
import CountriesApp from './CountriesApp';

test('form errors with searchType = name and value = blank', () => {
    expect(CountriesApp.getFormErrors("name", "")).toEqual(['Country input is invalid.']);
});

test('form errors with searchType = name and value = test', () => {
    expect(CountriesApp.getFormErrors("name", "test")).toEqual([]);
});

test('form errors with searchType = alpha and value = blank', () => {
    expect(CountriesApp.getFormErrors("alpha", "")).toEqual(['Country code must be 2 or 3 characters long.']);
});

test('form errors with searchType = alpha and value = a', () => {
    expect(CountriesApp.getFormErrors("alpha", "a")).toEqual(['Country code must be 2 or 3 characters long.']);
});

test('form errors with searchType = alpha and value = aaaa', () => {
    expect(CountriesApp.getFormErrors("alpha", "a")).toEqual(['Country code must be 2 or 3 characters long.']);
});

test('form errors with searchType = alpha and value = aa', () => {
    expect(CountriesApp.getFormErrors("alpha", "aa")).toEqual([]);
});

test('form errors with searchType = alpha and value = aaa', () => {
    expect(CountriesApp.getFormErrors("alpha", "aaa")).toEqual([]);
});