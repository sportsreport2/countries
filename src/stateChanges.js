
export const inputChange = (key, value) => (state) => ({
    [key]:value
});

export const loadingCountryData = (state) => ({
    loadingCountryData: true
});

export const doneLoadingCountryData = (state) => ({
    loadingCountryData: false
});

export const formErrors = (errors) => (state) => ({
    formErrors: errors
});

export const countryData = (countries, regionCounts, recordsTotal) => (state) => ({
    recordsTotal: recordsTotal,
    regionCounts: regionCounts,
    countries: countries,
    countryInputOnSubmit: state.countryInput,
    searchTypeOnSubmit: state.searchType
});