import React from 'react';
import ReactDOM from 'react-dom';
import ReactTestUtils from 'react-dom/test-utils'; // ES6
import * as stateChanges from './stateChanges'

test('input changes', () => {
    expect(stateChanges.inputChange('name', 'USA')({})).toEqual({'name':'USA'});
});

test('loading country data', () => {
    expect(stateChanges.loadingCountryData({})).toEqual({loadingCountryData: true});
});

test('done loading country data', () => {
    expect(stateChanges.doneLoadingCountryData({})).toEqual({loadingCountryData: false});
});

test('form errors', () => {
    expect(stateChanges.formErrors(['error1', 'error2'])()).toEqual({formErrors: ['error1', 'error2']});
});

test('country data', () => {
    expect(stateChanges.countryData([{name: 'USA'}], [{'americas': 1}], 1)({countryInput: 'USA', searchType: 'name'})).toEqual({
        recordsTotal: 1,
        regionCounts: [{'americas': 1}],
        countries: [{name: 'USA'}],
        countryInputOnSubmit: 'USA',
        searchTypeOnSubmit: 'name'
    });
});