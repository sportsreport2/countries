import React from 'react';
import ReactDOM from 'react-dom';
import CountriesTable from './CountriesTable';

test('countries table count info numberOfCountries = 1 and recordsTotal = 1', () => {
    expect(CountriesTable.getTableCountInfo(1, 1)).toBe("Showing 1 country");
});

test('countries table count info numberOfCountries = 10 and recordsTotal = 10', () => {
    expect(CountriesTable.getTableCountInfo(10, 10)).toBe("Showing 10 countries");
});

test('countries table count info numberOfCountries = 10 and recordsTotal = 50', () => {
    expect(CountriesTable.getTableCountInfo(10, 50)).toBe("Showing 10 countries (filtered from 50 total countries)");
});