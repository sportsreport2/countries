import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import CountriesApp from './CountriesApp';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<CountriesApp />, document.getElementById('root'));
registerServiceWorker();
