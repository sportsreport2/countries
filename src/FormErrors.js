import React, { PureComponent } from 'react';

class FormErrors extends PureComponent {
    render() {
        let { errors } = this.props;

        if(errors.length > 0) {
            return (<div className="alert alert-danger" role="alert">
                {errors.map((error, i) => (<p key={i}>{error}</p>))}
            </div>)
        }

        return '';
    }
}

export default FormErrors;