import React, { Component } from 'react';
import 'whatwg-fetch'
import * as stateChanges from './stateChanges';
import './CountriesApp.css';
import Navbar from './Navbar';
import CountrySearchForm from './CountrySearchForm';
import CountriesTable from './CountriesTable';
import RegionCounts from './RegionCounts';

class CountriesApp extends Component {
    constructor (props) {
        super(props);
        this.state = {
            countryInput: 'USA',
            countryInputOnSubmit: 'USA',
            loadingCountryData: false,
            searchType: 'name',
            searchTypeOnSubmit: 'name',
            formErrors: [],
            recordsTotal: null,
            regionCounts: [],
            countries: []
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        this.getCountryData();
    }

    handleInputChange = e => {
        this.setState(stateChanges.inputChange(e.target.name, e.target.value), () => this.validateForm());
    };

    handleSubmit = e => {
        //Prevent form from actually submitting
        e.preventDefault();
        //Only update state and fetch new data if the search is new
        //In the future, validate against cache rather than just previous input
        if(this.state.countryInputOnSubmit !== this.state.countryInput || this.state.searchTypeOnSubmit !== this.state.searchType) {
           this.setState(stateChanges.loadingCountryData, () => this.getCountryData());
        }
    };

    getCountryData() {
        if(this.state.formErrors.length === 0) {
            const searchType = this.state.searchType;
            const country = this.state.countryInput;
            let that = this;
            return fetch('http://localhost:8000/api.php/'+searchType+'/'+country, {
                method: "GET",
                "Content-Type": "application/json",
                "Accept": "application/json"
            }).then(
                response => response.json()
            ).then(json => {
                if(json.hasOwnProperty("status") && json.hasOwnProperty("message")) {
                    let errors = [];
                    if(json.status == "404") {
                        errors.push("No countries found")
                    } else {
                        errors.push("Error processing request");
                    }
                    that.setState(stateChanges.formErrors())
                }

                that.setState(stateChanges.doneLoadingCountryData);
                that.setState(stateChanges.countryData(json.countries, json.regionCounts, json.recordsTotal));
            }).catch(error => {
                that.setState(stateChanges.formErrors(["Error processing request"]));
                that.setState(stateChanges.doneLoadingCountryData);
            })
        }
    }

    validateForm() {
        let formErrors = CountriesApp.getFormErrors(this.state.searchType, this.state.countryInput);
        this.setState(stateChanges.formErrors(formErrors));
    }

    static getFormErrors(searchType, input) {
        let fieldValidationErrors = [];

        //Validate Country text input
        switch(searchType) {
            case "name":
                if(!/^.+$/.test(input)) {
                    fieldValidationErrors.push('Country input is invalid.');
                }
                break;

            case "alpha":
                if(!/^[a-z]{2,3}$/i.test(input)) {
                    fieldValidationErrors.push('Country code must be 2 or 3 characters long.');
                }
                break;
        }

        return fieldValidationErrors;
    }

    render() {
        return (
            <div>
                <div className={`loading ${(this.state.loadingCountryData ? '' : 'hidden')}`}>Loading&#8230;</div>
                <Navbar />
                <div className="container">
                    <CountrySearchForm handleFormSubmit={this.handleSubmit} handleInputChange={this.handleInputChange} formErrors={this.state.formErrors} countryInput={this.state.countryInput} searchType={this.state.searchType} handleSearchTypeChange={this.handleSearchTypeChange} />
                    <CountriesTable countries={this.state.countries} recordsTotal={this.state.recordsTotal} />
                    <RegionCounts regionCounts={this.state.regionCounts} />
                </div>
            </div>
        );
    }
}

export default CountriesApp;