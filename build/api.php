<?php

error_reporting(E_ALL & E_NOTICE & E_STRICT & E_DEPRECATED);

header("Access-Control-Allow-Origin: *");
header('Content-type:application/json;charset=utf-8');

//Create new API instance
$api = new API();
//Make API call
$apiResult = $api->apiCall();
//If result is false, return default response.
if(!$apiResult)
{
    echo json_encode(array(
        'status' => '404',
        'message' => 'Not Found',
        'recordsTotal' => 0,
        'regionCounts' => [],
        'countries' => []
    ));
}

//Decode the API result
$apiResult = json_decode($apiResult, true);

//Create a new Countries instance
$countries = new Countries($apiResult);
//Sort the countries
$countries->sort();
//Limit the countries
$countries->limit();
//Create the response
$response = array(
    'recordsTotal' => $countries->getCountryCount(),
    'regionCounts' => $countries->getRegionCounts(false),
    'countries' => $countries->getFilteredCountries(),
);
//Return the response
echo json_encode($response);

class API
{
    const API_URL = "https://restcountries.eu/rest/v2/";
    const DEFAULT_FIELDS = array(
        'name',
        'alpha2Code',
        'alpha3Code',
        'flag',
        'region',
        'subregion',
        'population',
        'languages'
    );
    const POSSIBLE_ENDPOINTS = array(
        'name',
        'alpha'
    );

    /** @var array $urlParameters */
    private $urlParameters = [];

    public function __construct($urlParameters = [])
    {
        $this->urlParameters = $urlParameters;
    }

    /**
     * This function executes a cURL request.
     * Check to make sure the URL is valid before making the cURL request.
     * Check to make sure the cURL response was valid.
     * Return the result.
     * @return bool|array
     */
    public function apiCall()
    {
        //Build URL
        $url = $this->buildURL();
        if(!$url)
        {
            return false;
        }

        //Get cURL resource
        $curl = curl_init();

        //Set options
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));

        //Send the request
        $result = curl_exec($curl);

        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if($this->isResponseValid($httpCode, $result))
        {
            //Close resource
            curl_close($curl);
            return $result;
        }

        //Close resource
        curl_close($curl);
        return false;
    }

    /**
     * This function builds the URL to be used in the cURL request.
     * The URL is comprised of the API_URL, two parameters separated by /, and a URL variable called fields.
     * Fields is a semicolon separated list of fields to be returned.
     * @return bool|string
     */
    public function buildURL()
    {
        //If URL parameters were not set in constructor, set them based on URL path info.
        if(count($this->urlParameters) <= 0)
        {
            $urlParameters = explode('/', trim($_SERVER['PATH_INFO'],'/'));
            $this->setUrlParameters($urlParameters);
        }

        //Validate url parameters
        if(!$this->areUrlParametersValid())
        {
            return false;
        }

        //Build URL
        return self::API_URL . $this->urlParameters[0] . "/" . $this->urlParameters[1] . "?fields=" . implode(";", self::DEFAULT_FIELDS);
    }

    /**
     * This function validates the url parameters based on the restcountries.eu API.
     * @return bool
     */
    public function areUrlParametersValid()
    {
        if(count($this->urlParameters) <= 0)
        {
            return false;
        }

        if(!in_array($this->urlParameters[0], self::POSSIBLE_ENDPOINTS))
        {
            return false;
        }

        switch($this->urlParameters[0])
        {
            //If first parameter is name, second parameter must be one or more characters.
            case "name":
                if(!preg_match('/^.+$/', $this->urlParameters[1]))
                {
                    return false;
                }
                break;

            //If first parameter is alpha, second paramter must be 2 or 3 letters long, case insensitive.
            case "alpha":
                if(!preg_match('/^[a-z]{2,3}$/i', $this->urlParameters[1]))
                {
                    return false;
                }
                break;
        }

        return true;
    }

    /**
     * This function checks a cURL response for validity.
     * If http code is greater than or equal 400 or the cURL result is false
     * or the JSON response from the API call contains a status and
     * that status is greater than or equal to 400 the response is invalid.
     * @param $httpCode
     * @param $result
     * @return bool
     */
    public function isResponseValid($httpCode, $result)
    {
        $decodedResult = json_decode($result, true);
        if(
            $httpCode >= 400 ||
            !$result ||
            (array_key_exists("status", $decodedResult) && $decodedResult['status'] >= 400))
        {
            return false;
        }

        return true;
    }

    /**
     * This function sets the urlParameters.
     * All URL parameters should be rawurlencoded to be used in a cURL request.
     * @param $urlParameters
     */
    public function setUrlParameters($urlParameters)
    {
        $urlParameters = array_map('rawurlencode', $urlParameters);
        $this->urlParameters = $urlParameters;
    }

    /**
     * This function returns the urlParameters array.
     * @return array
     */
    public function getUrlParameters()
    {
        return $this->urlParameters;
    }
}

class Countries
{
    const DEFAULT_LIMIT = 50;
    const DEFAULT_SORT = array(
        'name' => SORT_ASC,
        'population' => SORT_ASC
    );

    /** @var array $originalCountries */
    private $originalCountries = [];

    /** @var array $filteredCountries */
    private $filteredCountries = [];

    public function __construct($countries)
    {
        $this->setOriginalCountries($countries);
        $this->setFilteredCountries($countries);
    }

    /**
     * This function sorts the countries array based on the $fields parameter array.
     * The $fields parameter must be in this format:
     * array(
     *      'fieldName' => Sort Direction (SORT_ASC, SORT_DESC, SORT_REGULAR, SORT_NUMERIC, SORT_STRING)
     * )
     * @param array $fields
     */
    public function sort(array $fields = self::DEFAULT_SORT)
    {
        $parameters = [];
        foreach($fields as $field => $direction)
        {
            array_push($parameters, array_column($this->filteredCountries, $field));
            array_push($parameters, $direction);
        }

        array_push($parameters, $this->filteredCountries);

        call_user_func_array("array_multisort", $parameters);
    }

    /**
     * This function slices the countries array by the $limit parameter.
     * @param int $limit
     */
    public function limit($limit = self::DEFAULT_LIMIT)
    {
        $this->setFilteredCountries(array_slice($this->filteredCountries, 0, $limit));
    }

    /**
     * This function creates an array with region and sub region counts.
     * The array format is as follows:
     * region => (
     *      count => int
     *      subRegions => (
     *          subregion => (
     *              count => int
     *          )
     *      )
     * )
     * @param $useOriginal bool Region counts from original array or filtered array
     * @return array
     */
    public function getRegionCounts($useOriginal = true)
    {
        $countries = ($useOriginal ? $this->originalCountries : $this->filteredCountries);
        $regions = [];
        foreach($countries as $country)
        {
            if(array_key_exists('region', $country))
            {
                $country['region'] = ($country['region'] == "" ? "No Region" : $country['region']);
                $regions[$country['region']]['count'] = (!array_key_exists($country['region'], $regions) ? 1 : $regions[$country['region']]['count'] + 1);
            }

            if(array_key_exists('subregion', $country))
            {
                $country['subregion'] = ($country['subregion'] == "" ? "No Sub Region" : $country['subregion']);
                $regions[$country['region']]['subregions'][$country['subregion']]['count'] = (!array_key_exists($country['subregion'], $regions[$country['region']]['subregions']) ? 1 : $regions[$country['region']]['subregions'][$country['subregion']]['count'] + 1);
            }
        }

        return $regions;
    }

    /**
     * This function returns the number of countries in the original or
     * the filtered countries array based on the $useOriginal parameter.
     * @param $useOriginal bool Region counts from original array or filtered array
     * @return int
     */
    public function getCountryCount($useOriginal = true)
    {
        return count(($useOriginal ? $this->originalCountries : $this->filteredCountries));
    }

    /**
     * This function returns the filtered countries array.
     * @return array
     */
    public function getFilteredCountries()
    {
        return $this->filteredCountries;
    }

    /**
     * This function sets the filtered countries array.
     * @param array $filteredCountries
     */
    public function setFilteredCountries(array $filteredCountries)
    {
        $this->filteredCountries = $this->associativeToIndexedFirstLevel($filteredCountries);
    }

    /**
     * This function returns the original countries array.
     * @return array
     */
    public function getOriginalCountries()
    {
        return $this->originalCountries;
    }

    /**
     * This function sets the original countries array.
     * @param array $originalCountries
     */
    public function setOriginalCountries($originalCountries)
    {
        $this->originalCountries = $this->associativeToIndexedFirstLevel($originalCountries);
    }

    /**
     * This function checks the first level of an array to see if it is an associative or indexed array.
     * If it is an associative array, create a new array with the original array as its contents.
     * Therefore, the first level of the array is indexed vs associative.
     * @param $array
     * @return array
     */
    private function associativeToIndexedFirstLevel($array)
    {
        //Make sure first level is not an associative array
        //I.e. check to see if the key "0" exists
        if(!array_key_exists("0", $array))
        {
            return array(
                $array
            );
        }

        return $array;
    }

}