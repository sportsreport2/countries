## Table of Contents

- [Requirements](#requirements)
- [Deviations from Requirements](#deviations-from-requirements)
- [Getting Started](#getting-started)
- [Possible Improvements](#possible-improvements)

##Requirements

**Goal**

 - As a user, I want to search for countries by name, full name, or code
   and view them as a list of elements on an HTML page so that I can
   find the country data I am looking for.

**Acceptance Criteria**

- Use the Rest Countries api as your data source (https://restcountries.eu/). An HTML form input will accept the string name of a country.
- An error message will be emitted if users submit the form without input or if the search yields no results.
- The form data must be submitted via JS to retrieve and display the country or countries returned by the server. The page should not reload.
- The search results should be displayed on an HTML page, and be sorted alphabetically by the country’s name and population. Limit the api results to 50. Filtering, sorting, and limiting should be done in PHP and the Rest Countries service.
- For each country displayed include: the full name, alpha code 2, alpha code 3, flag image (scaled to fit display), region, subregion, population, and a list of its languages.
- At the bottom of the page show the total number of countries and list all regions and subregions contained in the results with the number of times it appeared.
- Using PHP create an API endpoint that will request data from Rest Countries. The PHP endpoint you build should return JSON and include all the data necessary to render the view as described.
- Your PHP code should work on PHP 7.0. or higher.
- Use React js for your javascript
- Your code does not need to work in older browsers but should work in the latest stable version of Chrome. 

##Deviations from Requirements

- Validation of the form is done on change of the dropdown or text box rather than on submit
	- This minimizes requests to the API by validating input first
	- All characters are allowed to be typed into the text box since the https://restcountries.eu/ accepts parentheses, Scandinavian characters, etc. 
		- Any errors will be caught by the https://restcountries.eu/ API rather than the front end
- The form cannot be submitted if the search is the same as the most recent search


## Getting Started 
*All commands below assume your current working directory is this project

####**Quick Start**

Dependencies

- PHP CLI >= 7.0 

```sh
php -S localhost:8000 -t build/
```
*Normally the build folder would not be included in the git repository but it is included for quicker access to application. 

####**Steps to run the application from scratch**

```sh
npm install
```
You’ll need to have Node >= 6 on your machine.

```sh
npm run build
```
Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

```sh
NODE_ENV=production npm start
``` 
or 
```sh 
php -S localhost:8000 -t build/
```
Starts the built-in PHP web server with the build folder as the document root directory.

##Possible Improvements

####Front end Improvements
- Add a second view with a map of the world. Highlight all countries present in the search results
- Find a better way to display the region counts to convey more information
	- Listing the countries that fall into each region along with counts
	- Breaking the regions into separate tables to minimize confusion 
- Add sorting and pagination to the countries table 
	- Return all results to the front end but only display 50 per page
	- Sorting to take place on the front end 
		- Each column could be sorted
- Allow the user to decide the number of results per page
- Cache the search results in state so if a search is repeated, there is not an additional API call 
	- A reset button would need to be added to the search form
- Save the state in local or session storage so refreshing does not delete results
- Remove the dependency on the local PHP server
- Add an environment script to determine the API route based on environment (dev/test/uat/prod)
- Changes to UI
	- Add a separate page for Region Counts so the user doesn't have to scroll to the bottom to view them
	- Add collapsible panels to the main page one for the countries table and one for region counts
- Refactor code to be more scalable to allow for future improvements
- Leverage existing packages to allow for quicker development and scalability
	- Ex. Use a react forms library to make the search form dynamic and the state consistent
- Leverage more ES6 improvements
- Move files in to a better directory structure to allow for scalability

####Back end Improvements
- Use a PHP framework that has routing built in to better structure code and allow for scalability and expansion of the API in the future
	- Based on the requirements, using a framework would add unnecessary overhead. The benefits are not worth it for such a small project.
- Add security to the API so there is not unwanted access
	- Setting Access-Control-Allow-Origin to * is not best practice
- Refactor the API to leverage a generic class for making API requests
- Leverage more PHP 7 improvements
- Split API and Countries classes into their own files

####General Improvements
- Improve unit tests
    - Test more functions of components from a react perspective
    - Add unit tests to the API (PHP Unit)
